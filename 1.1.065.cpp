#include<iostream>
using namespace std;

int main()
{
    int h1, a1, h2, a2;
    cin >> h1 >> a1 >> h2 >> a2;

    while (h1 > 0 && h2 > 0)
    {
        h2 -= a1;
        h1 -= a2;
    }
    
    if (h2 <= 0)
    {
        cout << "Bandergolf Cyberswitch";
    }
    else
    {
        cout << "Benadryl Cabbagepatch";
    }
}
