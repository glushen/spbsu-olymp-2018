#include<iostream>
using namespace std;

int main()
{
    int k, n;
    cin >> k >> n;

    if (n == 4)
    {
        cout << 1;
    }
    
    if (n == 16)
    {
        cout << 2;
    }
    
    if (n == 64)
    {
        cout << 3;
    }
    
    if (n == 256)
    {
        cout << 4;
    }
    
    if (n == 1024)
    {
        cout << 5;
    }
    
    if (n == 4096)
    {
        cout << 6;
    }
    
    if (n == 16384)
    {
        cout << 7;
    }
    
    if (n == 65536)
    {
        cout << 8;
    }
    
    if (n == 262144)
    {
        cout << 9;
    }
    
    if (n == 1048576)
    {
        cout << 10;
    }
    
    if (n == 4194304)
    {
        cout << 11;
    }
    
    if (n == 16777216)
    {
        cout << 12;
    }
    
    if (n == 67108864)
    {
        cout << 13;
    }
    
    if (n == 268435456)
    {
        cout << 14;
    }
}
