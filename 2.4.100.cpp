#include<iostream>
using namespace std;

long long divRoundUp(long long a, long long b)
{
    if (a % b == 0)
    {
        return a / b;
    }
    else
    {
        return a / b + 1;
    }
}

int main()
{
    long long k, n;
    cin >> k >> n;

    if (n <= k+1)
    {
        cout << 1;
    }
    else if (n <= (k+1)*(k+1))
    {
        cout << 2;
    }
    else
    {
        int r = 0;

        while (n > 1)
        {
            n = divRoundUp(n, k+1);
            r++;
        }

        cout << r;
    }
}
