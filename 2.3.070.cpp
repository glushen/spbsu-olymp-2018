#include<iostream>
using namespace std;

int divRoundUp(int a, int b)
{
    if (a % b == 0)
    {
        return a / b;
    }
    else
    {
        return a / b + 1;
    }
}

int main()
{
    int k, n;
    cin >> k >> n;

    if (n <= k+1)
    {
        cout << 1;
    }
    else if (n <= (k+1)*(k+1))
    {
        cout << 2;
    }
    else
    {
        int r = 0;

        while (n > 1)
        {
            n = divRoundUp(n, k+1);
            r++;
        }

        cout << r;
    }
}
