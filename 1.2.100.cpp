#include<iostream>
using namespace std;

int main()
{
    long long h1, a1, h2, a2;
    cin >> h1 >> a1 >> h2 >> a2;

    long long a1x100 = a1 * 100, a2x100 = a2 * 100;

    while (h1 > 0 && h2 > 0)
    {
        h2 -= a1x100;
        h1 -= a2x100;
    }
    
    h2 += a1x100;
    h1 += a2x100;

    while (h1 > 0 && h2 > 0)
    {
        h2 -= a1;
        h1 -= a2;
    }
    
    if (h2 <= 0)
    {
        cout << "Bandergolf Cyberswitch";
    }
    else
    {
        cout << "Benadryl Cabbagepatch";
    }
}
