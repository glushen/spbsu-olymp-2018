#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

vector<vector<int>> tree;

int dfs(int k, int v = 1, int pv = -1)
{
    int tvs = tree[v].size();

    vector<int> d;
    d.reserve(tvs);

    for (int i = 0; i < tvs; i++)
    {
        if (tree[v][i] != pv)
        {
            d.push_back(dfs(k, tree[v][i], v));
        }
    }

    if (d.size() < k)
    {
        return 1;
    }
    else
    {
        sort(d.begin(), d.end());
        return d[d.size()-k] + 1;
    }
}

vector<int> a;

void compute(int l, int r)
{
    if (r - l <= 1)
    {
        return;
    }
	
    if (a[l] == a[r])
    {
        for (int k = l+1; k < r; k++)
        {
            a[k] = a[l];
        }
    }
    else
    {
        int m = l + (r - l) / 2;

        a[m] = dfs(m);

	    compute(l, m);
	    compute(m, r);
    }
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    cin >> n;

    tree.resize(n+1);

    for (int i = 1; i < n; i++)
    {
        int u, v;
        cin >> u >> v;

        tree[u].push_back(v);
        tree[v].push_back(u);
    }

    a.resize(n+1);

    a[1] = dfs(1);
    a[n] = dfs(n);

    compute(1, n);

    for (int i = 1; i <= n; i++)
    {
        cout << a[i] << ' ';
    }
}