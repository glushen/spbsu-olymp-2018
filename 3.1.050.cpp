#include<iostream>
#include<vector>
using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    
    int n, m, p, q;
    cin >> n >> m >> p >> q;

    vector<vector<int>> a(n, vector<int>(m));

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            cin >> a[i][j];
        }
    }

    int s;
    cin >> s;

    int l, r, b, t;

    int v;
    cin >> v;

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (v == a[i][j])
            {
                l = r = j;
                b = t = i;
                break;
            }
        }
    }

    int result = 0;

    for (int i = 1; i < s; i++)
    {
        cin >> v;
        
        int xv, yv;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (v == a[i][j])
                {
                    xv = j;
                    yv = i;
                    break;
                }
            }
        }

        if (l > xv) l = xv;
        if (r < xv) r = xv;
        if (b > yv) b = yv;
        if (t < yv) t = yv;

        if (r - l + 1 > q || t - b + 1 > p)
        {
            result++;

            l = r = xv;
            b = t = yv;
        }
    }

    cout << result;
}