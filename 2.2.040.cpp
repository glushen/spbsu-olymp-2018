#include<iostream>
using namespace std;

int main()
{
    int k, n;
    cin >> k >> n;

    int t = k+1;
    int r = 1;

    while (t != n)
    {
        t *= k+1;
        r++;
    }

    cout << r;
}
