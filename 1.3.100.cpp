#include<iostream>
using namespace std;

int divRoundUp(int a, int b)
{
    if (a % b == 0)
    {
        return a / b;
    }
    else
    {
        return a / b + 1;
    }
}

int main()
{
    int h1, a1, h2, a2;
    cin >> h1 >> a1 >> h2 >> a2;
    
    if (divRoundUp(h2, a1) <= divRoundUp(h1, a2))
    {
        cout << "Bandergolf Cyberswitch";
    }
    else
    {
        cout << "Benadryl Cabbagepatch";
    }
}
