#include<iostream>
#include<vector>
using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    
    int n, m, p, q;
    cin >> n >> m >> p >> q;

    vector<int> x(n * m + 1), y(n * m + 1);

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            int v;
            cin >> v;

            x[v] = j;
            y[v] = i;
        }
    }

    int s;
    cin >> s;

    int l, r, b, t;

    int v;
    cin >> v;

    l = r = x[v];
    b = t = y[v];

    int result = 0;

    for (int i = 1; i < s; i++)
    {
        cin >> v;
        
        int xv = x[v];
        int yv = y[v];

        if (l > xv) l = xv;
        if (r < xv) r = xv;
        if (b > yv) b = yv;
        if (t < yv) t = yv;

        if (r - l + 1 > q || t - b + 1 > p)
        {
            result++;

            l = r = xv;
            b = t = yv;
        }
    }

    cout << result;
}